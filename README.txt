Team Members:
Chris Rohlfs - car2228
Debashish Sanyal - dhs2143
Devwrat More - dmm2228
Rohan Kulkarni - rk2845

Directory structure:
data: Directory for SVHN data.
data_CIFAR: Directory for CIFAR-10 data.
data_MNIST: Directory for MNIST data.
Baseclasses.py, ReNet.py, utils.py and mnist_read.py : Python code to reproduce the paper results. Implementation details are provided in the section III of the submitted report.

Steps to run the code:
1) Clone the project repository in a Theano environment .
2) Install scipy as : "sudo apt-get install python-scipy"
3) Install scikit-learn as : “pip install -U scikit-learn”
4) Import the functions and variables in ReNet.py as : “from ReNet import *”
5) Run test_renet() function using appropriate parameters as in **kwargs “params” in the code

