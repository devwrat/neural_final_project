"""
Source Code for Homework 3 of ECBM E6040, Spring 2016, Columbia University

This code contains implementation of several utility funtions for the homework.

Instructor: Prof. Aurel A. Lazar

This code is based on
[1] http://deeplearning.net/tutorial/logreg.html
"""
import os
import sys
import numpy
import scipy.io
import random
import cPickle as pickle
import urllib
from mnist_read import load_mnist

import theano
import theano.tensor as T


def shuffle(lol, seed):
    """
    Suffle inplace each list in the same order

    :type lol: list
    :param lol: list of list as input

    :type seed: int
    :param sedd: seed the shuffling

    """
    for l in lol:
        random.seed(seed)
        random.shuffle(l)


def shared_dataset(data_xy, borrow=True):
    """ Function that loads the dataset into shared variables

    The reason we store our dataset in shared variables is to allow
    Theano to copy it into the GPU memory (when code is run on GPU).
    Since copying data into the GPU is slow, copying a minibatch everytime
    is needed (the default behaviour if the data is not in a shared
    variable) would lead to a large decrease in performance.
    """
    data_x, data_y = data_xy
    shared_x = theano.shared(numpy.asarray(data_x,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    shared_y = theano.shared(numpy.asarray(data_y,
                                           dtype=theano.config.floatX),
                             borrow=borrow)
    # When storing data on the GPU it has to be stored as floats
    # therefore we will store the labels as ``floatX`` as well
    # (``shared_y`` does exactly that). But during our computations
    # we need them as ints (we use labels as index, and if they are
    # floats it doesn't make sense) therefore instead of returning
    # ``shared_y`` we will have to cast it to int. This little hack
    # lets ous get around this issue
    return shared_x, T.cast(shared_y, 'int32')
def load_data(ds_rate=None, theano_shared=True,dataset_used = 'SVHN'):
    ''' Loads the SVHN dataset

    :type ds_rate: float
    :param ds_rate: downsample rate; should be larger than 1, if provided.

    :type theano_shared: boolean
    :param theano_shared: If true, the function returns the dataset as Theano

    :type dataset_used: string
    :param dataset_used: Loads the dataset as per user's choice (SHVN or CIFAR-10)


    shared variables. Otherwise, the function returns raw data.
    '''
    if ds_rate is not None:
        assert(ds_rate > 1.)

    # Download the SVHN dataset if it is not present
    def check_dataset(dataset):
        # Check if dataset is in the data directory.
        if (dataset_used == 'SVHN'):
            new_path = os.path.join(
                os.path.split(__file__)[0],
                "data",
                dataset
                )

        if (dataset_used == 'CIFAR-10'):
            new_path = os.path.join(
                os.path.split(__file__)[0],
                dataset
                )

        if (dataset_used == 'MNIST'):
            new_path = os.path.join(
                os.path.split(__file__)[0],
                "data_MNIST",
                dataset
                )

        #Download if data is not in the directory.
        if (not os.path.isfile(new_path)):
            from six.moves import urllib
            if (dataset_used == 'SVHN'):
                origin = (
                    'http://ufldl.stanford.edu/housenumbers/' + dataset
                    )
            if (dataset_used == 'CIFAR-10'):
                origin = (
                    'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz'
                    )
            if (dataset_used == 'MNIST'):
                origin = (
                    'http://yann.lecun.com/exdb/mnist/' + dataset
                    )
            print('Downloading data from %s' % origin)
            urllib.request.urlretrieve (origin, new_path)

            #CIFAR-10 dataset needs to be Unzipped before it can be used.
            if (dataset_used == 'CIFAR-10'):
                print('Unzipping from CIFAR-10')
                import tarfile
                if (new_path.endswith("tar.gz")):
                    tar = tarfile.open(new_path)
                    tar.extractall("data_CIFAR")
                    tar.close()
                    print "Extracted in Current Directory"
            #CIFAR-10 dataset needs to be Unzipped before it can be used.
            if (dataset_used == 'MNIST'):
                print('Unzipping from MNIST')
                import gzip
                inF = gzip.GzipFile(new_path, 'rb')
                s = inF.read()
                inF.close()

                outF = file(new_path[:-3], 'wb')
                outF.write(s)
                outF.close()



        return new_path

    #Check if data exists, else download data
    if dataset_used == 'SVHN':
        train_dataset = check_dataset('train_32x32.mat')
        test_dataset = check_dataset('test_32x32.mat')

    if dataset_used == 'CIFAR-10':
        check_dataset('data_CIFAR/cifar-10-python.tar.gz')

    if dataset_used == 'MNIST':
        check_dataset('train-images-idx3-ubyte.gz')
        check_dataset('train-labels-idx1-ubyte.gz')
        check_dataset('t10k-images-idx3-ubyte.gz')
        check_dataset('t10k-labels-idx1-ubyte.gz')



    # Load the dataset
    if dataset_used == 'SVHN':
        train_set = scipy.io.loadmat(train_dataset)
        test_set = scipy.io.loadmat(test_dataset)

    if dataset_used == 'CIFAR-10':
        x_train, y_train, x_test, y_test = load_cifar_data('data_CIFAR/cifar-10-batches-py')

        x_back = numpy.swapaxes(x_train,0,2)
        x_back = numpy.swapaxes(x_back,1,3)
        x_train = numpy.swapaxes(x_back,2,3)
        y_train = y_train.reshape(y_train.shape[0],1)

        x_back = numpy.swapaxes(x_test,0,2)
        x_back = numpy.swapaxes(x_back,1,3)
        x_test = numpy.swapaxes(x_back,2,3)
        y_test = y_test.reshape(y_test.shape[0],1)

        train_set = {'X':x_train,'y':y_train}
        test_set = {'X':x_test,'y':y_test}



    if dataset_used == 'MNIST':
        x, y_train = load_mnist(dataset = "training",path = "data_MNIST")
        #current state 60000,28,28 numpy
        #target state (28, 28, 1, 60000)
        x = x.reshape(x.shape[0],x.shape[1],x.shape[2],1)
        #now (60000,28,28,1)
        x_back = numpy.swapaxes(x,0,1)
        x_back = numpy.swapaxes(x_back,1,2)
        x_train = numpy.swapaxes(x_back,2,3)
        #finally (28,28,1,60000)

        x, y_test = load_mnist(dataset = "testing",path = "data_MNIST")
        x = x.reshape(x.shape[0],x.shape[1],x.shape[2],1)
        #now (60000,28,28,1)
        x_back = numpy.swapaxes(x,0,1)
        x_back = numpy.swapaxes(x_back,1,2)
        x_test = numpy.swapaxes(x_back,2,3)

        train_set = {'X':x_train,'y':y_train}
        test_set = {'X':x_test,'y':y_test}





    # Convert data format
    def convert_data_format(data):
        """
        X = numpy.reshape(data['X'],
                          (numpy.prod(data['X'].shape[:-1]), data['X'].shape[-1]),
                          order='C').T / 255.
        #print data['X'].shape
        #print X.shape
        x_back = numpy.reshape(X.T/1,(32,32,3,X.shape[0]))
        x_back = numpy.swapaxes(x_back,2,3)
        x_back = numpy.swapaxes(x_back,1,2)
        X = numpy.swapaxes(x_back,0,1)
        """
        X = data['X'].transpose(2,0,1,3)
        #X = data['X']
        X = X.reshape((numpy.prod(X.shape[:-1]), X.shape[-1]),order='C').T / 255.
        y = data['y'].flatten()
        y[y == 10] = 0
        return (X,y)
    train_set = convert_data_format(train_set)
    test_set = convert_data_format(test_set)

    # Downsample the training dataset if specified
    train_set_len = len(train_set[1])
    if ds_rate is not None:
        train_set_len = int(train_set_len // ds_rate)
        train_set = [x[:train_set_len] for x in train_set]

    # Extract validation dataset from train dataset
    valid_set = [x[-(train_set_len//10):] for x in train_set]
    train_set = [x[:-(train_set_len//10)] for x in train_set]
    #test_set = [x for x in test_set]
    test_set = list(test_set)


    # train_set, valid_set, test_set format: tuple(input, target)
    # input is a numpy.ndarray of 2 dimensions (a matrix)
    # where each row corresponds to an example. target is a
    # numpy.ndarray of 1 dimension (vector) that has the same length as
    # the number of rows in the input. It should give the target
    # to the example with the same index in the input.

    if theano_shared:
        test_set_x, test_set_y = shared_dataset(test_set)
        valid_set_x, valid_set_y = shared_dataset(valid_set)
        train_set_x, train_set_y = shared_dataset(train_set)

        rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    else:
        rval = [train_set, valid_set, test_set]

    return rval

#source https://github.com/Semantive/deep-learning-examples/blob/master/python/cifar.py
def unpickle(file):
    """
    Unpickles the file for CIFAR-10 dataset

    :type file: string
    :param file: file to be unpickled

    """
    f = open(file, 'rb')
    dict = pickle.load(f)
    f.close()
    return dict

#source https://github.com/Semantive/deep-learning-examples/blob/master/python/cifar.py
def load_cifar_data(path):
    """
    Loads CIFAR-10 dataset

    :type path: string
    :param path: directory where the data is stored. (e.g. data_CIFAR/cifar-10-batches-py)

    """
    x_train = numpy.zeros((50000, 3, 32, 32), dtype='uint8')
    y_train = numpy.zeros((50000,), dtype="uint8")

    for i in range(1, 6):
        data = unpickle(os.path.join(path, 'data_batch_' + str(i)))
        images = data['data'].reshape(10000, 3, 32, 32)
        labels = data['labels']
        x_train[(i - 1) * 10000:i * 10000, :, :, :] = images
        y_train[(i - 1) * 10000:i * 10000] = labels

    test_data = unpickle(os.path.join(path, 'test_batch'))
    x_test = test_data['data'].reshape(10000, 3, 32, 32)
    y_test = numpy.array(test_data['labels'])

    return x_train, y_train, x_test, y_test
