"""
Part of the code here is based on:
    Source Code for Homework 3 of ECBM E6040, Spring 2016, Columbia University
    Instructor: Prof. Aurel A. Lazar

This code is based on
[1] http://deeplearning.net/tutorial/logreg.html
[2] http://deeplearning.net/tutorial/mlp.html
[3] http://deeplearning.net/tutorial/lenet.html
"""

from __future__ import print_function

import timeit
import inspect
import sys
import numpy

import theano
import theano.tensor as T
from theano.tensor.nnet import conv2d
from theano.tensor.signal import downsample

import scipy

'''
This class entails a vertical unit followed by a horizontal unit.
Each unit sweeps the image along vertical and horizontal axes respectively.
Also, each sweep has two dimensions: Forward (Top-down / Left-right) and Backward (Bottom-up / Right-left)
'''
class HiddenLayerRNN(object):

    def __init__(self, input_x, n_in = None, image_w = 32,image_h = 32, channels = 3,
    nh = None,wp = None,hp = None,rng = None,activation=T.tanh):

        """
        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
                      architecture (one row)

        :type n_in: int
        :param n_in: patch size (calculated as channels*wp*wh in the calling object)

        :type image_w: int
        :param image_w: width of input 'image'

        :type image_h: int
        :param image_h: height of input 'image'

        :type channels: int
        :param channels: number of channels of input 'image'

        :type nh: int
        :param nh: number of output neurons of each hidden RNN layer.

        :type wp: int
        :param wp: width of patch

        :type hp: int
        :param hp: height of patch

        :type rng: rng.RandomState
        :param rng: a random number generator used to initialize weights

        :type nb: int
        :param nb: number of bits in input

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
        """
        #Number of patch rows.
        I = image_w//wp

        #Number of patch columns
        J = image_h//hp

        self.wx_top_down = theano.shared(name='wx_top_down',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (n_in, nh))
                            .astype(theano.config.floatX))
        self.wx_bottom_up = theano.shared(name='wx_bottom_up',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (n_in, nh))
                            .astype(theano.config.floatX))
        self.wx_left_right = theano.shared(name='wx_left_right',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (2*nh, nh))
                            .astype(theano.config.floatX))
        self.wx_right_left = theano.shared(name='wx_right_left',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (2*nh, nh))
                            .astype(theano.config.floatX))
        self.wh_top_down = theano.shared(name='wh_top_down',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (nh, nh))
                            .astype(theano.config.floatX))
        self.wh_bottom_up = theano.shared(name='wh_bottom_up',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (nh, nh))
                            .astype(theano.config.floatX))
        self.wh_left_right = theano.shared(name='wh_left_right',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (nh, nh))
                            .astype(theano.config.floatX))
        self.wh_right_left = theano.shared(name='wh_right_left',
                            value=0.2 * rng.uniform(-1.0, 1.0,
                            (nh, nh))
                            .astype(theano.config.floatX))
        self.h0_top_down = theano.shared(name='h0_top_down',
                            value=numpy.zeros((J,nh),
                            dtype=theano.config.floatX))
        self.h0_bottom_up = theano.shared(name='h0_bottom_up',
                            value=numpy.zeros((J,nh),
                            dtype=theano.config.floatX))
        self.h0_left_right = theano.shared(name='h0_left_right',
                            value=numpy.zeros((I,nh),
                            dtype=theano.config.floatX))
        self.h0_right_left = theano.shared(name='h0_right_left',
                            value=numpy.zeros((I,nh),
                            dtype=theano.config.floatX))

        #Parameters to be learned
        self.params = [self.wx_top_down,self.wx_bottom_up, self.wx_left_right,self.wx_right_left, self.wh_top_down, self.wh_bottom_up,
                       self.wh_left_right,self.wh_right_left,self.h0_top_down,self.h0_bottom_up,self.h0_left_right,self.h0_right_left]

        #Input is reshaped to facilitate sweeping across patches as per given patch dimensions.
        x = input_x.reshape((I, J, channels * wp * hp))

        #Vertical sweeps recurrence (UpDown and DownUp)
        def recurrence_v_ud(row_number,h_tm_ud,h_tm_du,x_t):
            h_t1 = T.maximum(T.dot(x_t[row_number,:,:], self.wx_top_down)
                             + T.dot(h_tm_ud, self.wh_top_down),0)

            h_t2 = T.maximum(T.dot(x_t[x.shape[0]-row_number-1,:,:], self.wx_bottom_up)
                             + T.dot(h_tm_du, self.wh_bottom_up),0)

            return h_t1,h_t2

        #Vertical sweep scan that calls the Vertical recurrence.
        [h_top_down, h_bottom_up],_1 = theano.scan(fn=recurrence_v_ud,
                            outputs_info= [self.h0_top_down,self.h0_bottom_up],
                            non_sequences = x,
                            sequences = T.arange(x.shape[0]),
                            )

        #Concatenating results
        self.h_vertical_out = T.concatenate([h_top_down,h_bottom_up],axis = 2)

        #No reshape required, output is already of the form IxJx(nout_RNN)
        x = self.h_vertical_out

        #Horizontal sweep recurrennce (Left to Right and Right to Left)
        def recurrence_h_left_right(col_number,h_tm1_lr,h_tm1_rl,x_t):

            h_t1 = T.maximum(T.dot(x_t[:,col_number,:], self.wx_left_right)
                             + T.dot(h_tm1_lr, self.wh_left_right),0)

            h_t2 = T.maximum(T.dot(x_t[:,x.shape[1] - 1 - col_number,:], self.wx_right_left)
                             + T.dot(h_tm1_rl, self.wh_right_left),0)

            return h_t1,h_t2

        #Horizontal sweep scan that calls the horizontal recurrence.
        [h_left_right,h_right_left], _1 = theano.scan(fn=recurrence_h_left_right,
                            outputs_info=[self.h0_left_right,self.h0_right_left],
                            non_sequences = x,
                            sequences = T.arange(x.shape[1]))

        h_horizontal_out = T.concatenate([h_left_right,h_right_left],axis = 2)

        #Output of the RNN layer
        self.out = h_horizontal_out

class LogisticRegression(object):
    """Multi-class Logistic Regression Class

    The logistic regression is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`. Classification is done by projecting data
    points onto a set of hyperplanes, the distance to which is used to
    determine a class membership probability.
    """

    def __init__(self, input, n_in, n_out):
        """ Initialize the parameters of the logistic regression

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
                      architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
                     which the datapoints lie

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
                      which the labels lie

        """
        # initialize with 0 the weights W as a matrix of shape (n_in, n_out)
        self.W = theano.shared(
            value=numpy.zeros(
                (n_in, n_out),
                dtype=theano.config.floatX
            ),
            name='W',
            borrow=True
        )
        # initialize the biases b as a vector of n_out 0s
        self.b = theano.shared(
            value=numpy.zeros(
                (n_out,),
                dtype=theano.config.floatX
            ),
            name='b',
            borrow=True
        )

        # symbolic expression for computing the matrix of class-membership
        # probabilities
        # Where:
        # W is a matrix where column-k represent the separation hyperplane for
        # class-k
        # x is a matrix where row-j  represents input training sample-j
        # b is a vector where element-k represent the free parameter of
        # hyperplane-k
        self.p_y_given_x = T.nnet.softmax(T.dot(input, self.W) + self.b)

        # symbolic description of how to compute prediction as class whose
        # probability is maximal
        self.y_pred = T.argmax(self.p_y_given_x, axis=1)

        # parameters of the model
        self.params = [self.W, self.b]

        # keep track of model input
        self.input = input

    def negative_log_likelihood(self, y):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|}
                \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
            \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -T.mean(T.log(self.p_y_given_x)[T.arange(y.shape[0]), y])

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.neq(self.y_pred, y))
        else:
            raise NotImplementedError()

class HiddenLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).
        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type rng: rng.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer

        """
        self.input = input

        # `W` is initialized with `W_values` which is uniformely sampled
        # from sqrt(-6./(n_in+n_hidden)) and sqrt(6./(n_in+n_hidden))
        # for tanh activation function
        # the output of uniform if converted using asarray to dtype
        # theano.config.floatX so that the code is runable on GPU
        # Note : optimal initialization of weights is dependent on the
        #        activation function used (among other things).
        #        For example, results presented in [Xavier10] suggest that you
        #        should use 4 times larger initial weights for sigmoid
        #        compared to tanh
        #        We have no info for other function, so we use the same as
        #        tanh.
        if W is None:
            W_values = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4

            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b

        lin_output = T.dot(input, self.W) + self.b
        self.output = (
            lin_output if activation is None
            else activation(lin_output)
        )
        # parameters of the model
        self.params = [self.W, self.b]
