'''
This file contains the following classes and functions:
CLASSES:
    ReNet : Builds the model architecture by initializing multiple ReNet layers and fully connected MLP layers

FUNCTIONS:
    ---------------------------------
    shifting() : Data augmentation by probabilistic shifting of every image horizontally and vertically by 2 pixels or not changing.
    flipping() : Data augmentation by probabilistic flipping of every image horizontally, vertically or not changing.
    test_renet() : Base function to test the ReNet architecture.
'''

from __future__ import print_function
from utils import load_data, shared_dataset, shuffle
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
import numpy
import timeit
import random
from operator import sub
import copy
from BaseClasses import HiddenLayerRNN,LogisticRegression,HiddenLayer
from sklearn import preprocessing
import sys
import six.moves.cPickle as pickle
import os
sys.setrecursionlimit(1500)


'''
Wrapper class to support the ReNet architecture. Contains multiple ReNet layers
each having a vertical scan layer and a horizontal scan layer. After the ReNet layers
come multiple fully connected MLP layers followed by the Logistic Regression layer that
outputs the classes for every example. Contains initialization of other vital
variables like the negative_log_likelihood, errors and collection of all parameters
for the entire model.

'''
class ReNet:
    def __init__(self, input_x,n_in = 3*2*2,nout_RNN = 5,n_RNNs = 1,wp = 2,hp = 2,
    channels = 3,rng = None,n_hidden = 512,
    image_w = 32,image_h = 32,n_out = 10,n_hiddenLayers=1):

        """
        NOTE : As specified in the paper ,the nonlinearity used here is tanh

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input_x: theano.tensor.dmatrix
        :param input_x: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_RNNs: int
        :param n_RNNs: number of ReNet layers in the model architecture.

        :type n_hiddenLayers: int
        :param n_hiddenLayers: number of fully connected hidden MLP layers.

        :type n_out: int
        :param n_out: number of output units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden layer

        :type channels: int
        :param channels: number of channels in every image of the dataset.

        :type wp: int
        :param wp: width of the patches

        :type hp: int
        :param hp: height of the patches

        :type image_w: int
        :param image_w: width of the images in the dataset

        :type image_h: int
        :param image_h: height of the images in the dataset

        """

        # Create vertical and horizontal layers to sweep the image patches.
        self.hiddenRNNs = []
        for i in xrange(n_RNNs):
            in_x = input_x if i == 0 else self.hiddenRNNs[i-1].out
            n_channels = channels if i == 0 else 2*nout_RNN
            n_patch_size = n_in if i == 0 else n_channels*wp*hp
            i_w = image_w if i == 0 else i_w//wp
            i_h = image_h if i == 0 else i_h//hp
            self.hiddenRNNs.append(
                HiddenLayerRNN(
                    rng=rng,
                    input_x = in_x,
                    channels = n_channels, #Corresponds to n_in
                    nh = nout_RNN,
                    n_in = n_patch_size,
                    image_w = i_w,
                    image_h = i_h,
                    wp = wp,
                    hp = hp,
                    activation=T.tanh
            ))

        n_in = (i_w//wp)*(i_h//hp)*2*nout_RNN;

        # Flatten the output so that it can be fed in to the MLP layer.
        hidden_input = T.reshape(self.hiddenRNNs[-1].out,(1,n_in))

        # Create MLP layer
        self.hiddenLayers = []
        for i in xrange(n_hiddenLayers):
            h_input = hidden_input if i == 0 else self.hiddenLayers[i-1].output
            h_in = n_in if i == 0 else n_hidden
            self.hiddenLayers.append(
                HiddenLayer(
                    rng=rng,
                    input=h_input,
                    n_in=h_in,
                    n_out=n_hidden,
                    activation=T.tanh
            ))

        # Create a regression layer to generate classification label.
        self.logRegressionLayer = LogisticRegression(
            input=self.hiddenLayers[-1].output,
            n_in=n_hidden,
            n_out=n_out
        )

        self.py  = self.logRegressionLayer.p_y_given_x

        # keep track of model input
        self.input = input

        # negative log likelihood of the MLP is given by the negative
        # log likelihood of the output of the model, computed in the
        # logistic regression layer
        self.nll = (
            self.logRegressionLayer.negative_log_likelihood
        )

        # errors are basically same as errors of the classification layer.
        self.errors = self.logRegressionLayer.errors

        # classification logic is similar to what we do in Logistic Regression Layer.
        self.classify = T.argmax(self.logRegressionLayer.p_y_given_x, axis=1)

        # Add all the parameters for backprogogation based learning
        self.params = sum([x.params for x in self.hiddenRNNs], []) + sum([x.params for x in self.hiddenLayers], []) + self.logRegressionLayer.params


# Function for augmenting the dataset using Shifting.
def shifting(train_set,channels,width,height):
    """
    :type train_set: numpy.ndarray
    :param train_set: training data to be augmented

    """
    # Reshape every example into an appropriate shape for flipping
    r,c = train_set.shape[0],train_set.shape[1]
    new_train_set = numpy.reshape(train_set.T/1,(channels,width,height,r))

    # Following lists are used for simulation of probabilities in both directions
    horizontal_prob = ['left','right','nochange','nochange']
    vertical_prob = ['up','down','nochange','nochange']

    for i in xrange(r):
        # Shifting horizontally by 2 bits with probability 25% for a left shift, 25% for a right shift and 50% for a no shift
        horz_shift = horizontal_prob[random.randint(0,3)]
        if horz_shift == 'left':
            for j in xrange(channels):
                new_train_set[j,:,:-2,i] = new_train_set[j,:,2:,i]
                new_train_set[j,:,-1,i] = 0 * new_train_set[j,:,-1,i]
                new_train_set[j,:,-2,i] = 0 * new_train_set[j,:,-1,i]

        elif horz_shift == 'right':
            for j in xrange(channels):
                new_train_set[j,:,2:,i] = new_train_set[j,:,:-2,i]
                new_train_set[j,:,0,i] = 0 * new_train_set[j,:,0,i]
                new_train_set[j,:,1,i] = 0 * new_train_set[j,:,1,i]

        # Further shifting horizontally by 2 bits with probability 25% for a up shift, 25% for a down shift and 50% no shift
        vert_shift = vertical_prob[random.randint(0,3)]
        if vert_shift == 'up':
            for j in xrange(channels):
                new_train_set[j,:-2,:,i] = new_train_set[j,2:,:,i]
                new_train_set[j,-1,:,i] = 0 * new_train_set[j,-1,:,i]
                new_train_set[j,-2,:,i] = 0 * new_train_set[j,-1,:,i]

        if vert_shift == 'down':
            for j in xrange(channels):
                new_train_set[j,2:,:,i] = new_train_set[j,:-2,:,i]
                new_train_set[j,0,:,i] = 0 * new_train_set[j,0,:,i]
                new_train_set[j,1,:,i] = 0 * new_train_set[j,1,:,i]

    return numpy.reshape(new_train_set,(numpy.prod(new_train_set.shape[:-1]), new_train_set.shape[-1]),order='C').T/1

# Function for augmenting the dataset using Flipping.
def flipping(train_set,channels,width,height):
    """
    :type train_set: numpy.ndarray
    :param train_set: training data to be augmented

    """
    # Reshape every example into an appropriate shape for flipping
    r,c = train_set.shape[0],train_set.shape[1]
    new_train_set = numpy.reshape(train_set.T/1,(channels,width,height,r))

    # Following list is used for simulation of probabilities
    flip_probs = ['horizontal','vertical','nochange','nochange']

    for i in xrange(r):
        # Select a direction for flipping with probability 25% for horizontal,
        # 25% for vertical and 50% for no flipping in the image.
        flip = flip_probs[random.randint(0,3)]
        if flip == 'horizontal':
            for j in xrange(channels):
                new_train_set[j,:,:,i] = numpy.fliplr(new_train_set[j,:,:,i])

        elif flip == 'vertical':
            for j in xrange(channels):
                new_train_set[j,:,:,i] = numpy.flipud(new_train_set[j,:,:,i])

    return numpy.reshape(new_train_set,(numpy.prod(new_train_set.shape[:-1]), new_train_set.shape[-1]),order='C').T/1

# Entry point of execution.
# When called with appropriate parameters, creates a ReNet and uses it for learning and prediction over specified dataset.
def test_renet(**kwargs):
    """
    :type learning_rate: float
    :param learning_rate: learning rate used (factor for the stochastic gradient.

    :type nepochs: int
    :param nepochs: maximal number of epochs to run the optimizer.

    :type npatch: list of tuples
    :param npatch: list of (width,height) denoting patch sizes for every ReNet layer.

    :type n_hidden: int
    :param n_hidden: number of hidden units.

    :type n_hiddenLayers: int
    :param n_hiddenLayers: number of fully connected hidden MLP layers.

    :type n_RNNs: int
    :param n_RNNs: number of ReNet layers in the model architecture.

    :type ds_rate: int
    :param ds_rate: downsampling rate for loading the data.

    :type channels: int
    :param channels: number of channels in every image of the dataset.

    :type shifting: boolean
    :param shifting: augment the dataset by shifting the images or not.

    :type flipping: boolean
    :param flipping: augment the dataset by flipping the images or not.

    :type mormalize_pixels: boolean
    :param normalize_pixels: normalize the dataset across pixels to have 0 mean and unit variance or not.

    :type wp: int
    :param wp: width of the patches

    :type hp: int
    :param hp: height of the patches

    :type verbose: boolean
    :param verbose: to print out epoch summary or not to.

    :type image_w: int
    :param image_w: width of the images in the dataset

    :type image_h: int
    :param image_h: height of the images in the dataset

    :type decay: boolean
    :param decay: decay on the learning rate if improvement stop.

    :type savemodel: boolean
    :param savemodel: save the trained model or not.

    :type batch_size: int
    :param batch_size: size of the batches on which training is done.

    :type dataset_used: string
    :param dataset_used: To choose between the 3 datasets (CIFAR-10,SVHN,MNIST). Default SVHN

    """

    param = {
        'learning_rate': 0.1,
        'verbose': True,
        'decay': True,
        'npatch': [(2, 2), (2, 2), (2, 2)],
        'seed': 345,
        'ds_rate':10,
        'nepochs': 60,
        'savemodel': False,
        'channels':3,
        'shifting':False,
        'normalize_pixels':True,
        'flipping':False,
        'dataset_used':'SVHN',
        'wp' : 2,
        'hp' : 2,
        'image_w':32,
        'image_h':32,
        'n_RNNs':1,
        'minibatch_size':1,
        'nout_RNN': 2,
        'n_hiddenLayers':1,
        'n_hidden':50,
        'batch_size': 200}

    param_diff = set(kwargs.keys()) - set(param.keys())
    if param_diff:
        raise KeyError("invalid arguments:" + str(tuple(param_diff)))
    param.update(kwargs)

    if param['verbose']:
        for k,v in param.items():
            print("%s: %s" % (k,v))

    lr = param['learning_rate']
    seed = param['seed']
    verbose = param['verbose']
    wp = param['wp']
    hp = param['hp']
    channels = param['channels']
    ds_rate = param['ds_rate']
    n_RNNs = param['n_RNNs']
    image_w = param['image_w']
    image_h = param['image_h']
    nout_RNN = param['nout_RNN']
    n_hidden = param['n_hidden']
    n_hiddenLayers = param['n_hiddenLayers']
    minibatch_size = param['minibatch_size']
    dataset_used = param['dataset_used']
    nepochs = param['nepochs']
    batch_size = param['batch_size']

    # Since MNIST data have different dimensions.
    if(dataset_used == "MNIST"):
        image_w = 28
        image_h = 28
        channels = 1

    nclasses = 10
    npatch = param['npatch']

    #### LOADING DATA ####
    train_set, valid_set, test_set = load_data(ds_rate=ds_rate, dataset_used = dataset_used,theano_shared=False)

    if param['flipping']:
        train_set[1] = numpy.tile(train_set[1], 2)
        train_set_x_flipped = flipping(train_set[0],channels,image_w,image_h)
        train_set[0] = numpy.vstack((train_set[0],train_set_x_flipped))

    if param['shifting']:
        train_set[1] = numpy.tile(train_set[1], 2)
        train_set_x_shifted = shifting(train_set[0],channels,image_w,image_h)
        train_set[0] = numpy.vstack((train_set[0],train_set_x_shifted))

    if param['normalize_pixels']:
        train_set[0] = preprocessing.scale(train_set[0])
        valid_set[0] = preprocessing.scale(valid_set[0])
        test_set[0] = preprocessing.scale(test_set[0])

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    test_set1 = test_set_y.eval()
    valid_set1 = valid_set_y.eval()

    n_train = train_set_x.get_value(borrow=True).shape[0]
    n_valid = valid_set_x.get_value(borrow=True).shape[0]
    n_test = test_set_x.get_value(borrow=True).shape[0]

    #### BUILDING MODEL ####
    print('... building the model')

    index = T.lscalar()
    #x = T.ftensor3('x')
    x = T.vector('x')
    y = T.ivector('y')

    rng = numpy.random.RandomState(1234)

    #Testing the ReNet
    rnn = ReNet(
        input_x= x,
        n_in= channels*wp*hp, #patch size i.e. total number of elements in one patch
        n_RNNs = n_RNNs,
        channels = channels,
        image_w = image_w,
        image_h = image_h,
        nout_RNN = nout_RNN,
        n_hiddenLayers = n_hiddenLayers,
        n_hidden = n_hidden,
        wp = wp,
        hp = hp,
        rng = rng,
        )

    print('Defining cost..')
    cost = rnn.nll(y)
    print('Cost done, calculating gradient..')
    gradients = T.grad(cost, rnn.params)
    print('Gradient done, setting updates..')
    updates = [ (p, p - lr*g) for p, g in zip(rnn.params, gradients)]

    print('Creating train model..')
    train_model = theano.function(inputs=[index],
                                          outputs=cost,
                                          updates=updates,
                                          givens={
                                              x: train_set_x[index],
                                              y: train_set_y[index:index+1]
                                          }
                                  )

    print('Creating validate model..')
    validate_model = theano.function(
        inputs=[index],
        outputs=rnn.classify,
        givens={
            x: valid_set_x[index],
        }
    )

    print('Creating test model..')
    test_model = theano.function(
        inputs=[index],
        outputs=rnn.classify,
        givens={
            x: test_set_x[index],
        }
    )

    print('Starting numerical computations..')
    best_f1 = -numpy.inf

    train_indexes = [i for i in xrange(n_train)]
    for e in range(param['nepochs']):

        # shuffle
        shuffle([train_indexes], param['seed'])

        param['ce'] = e
        tic = timeit.default_timer()

        for i  in xrange(n_train):
            train_model(train_indexes[i])
            print('[learning] epoch %i >> %2.2f%%' % (e, (i + 1) * 100. / n_train), end = ' ')
            print('completed in %.2f (sec) <<\r' % (timeit.default_timer() - tic), end='')
            sys.stdout.flush()

        # evaluation
        predictions_test = numpy.array([])
        for i in xrange(n_test):
            predictions_test = numpy.append(predictions_test,test_model(i))

        predictions_valid = numpy.array([])
        for i in xrange(n_valid):
            predictions_valid = numpy.append(predictions_valid,validate_model(i))

        # compute the accuracy
        res_test = ((n_test - sum(abs(predictions_test == test_set1)))/(n_test*1.))*100.
        res_valid = ((n_valid - sum(abs(predictions_valid == valid_set1)))/(n_valid*1.))*100.

        if res_valid > best_f1:

            be = e
            best_rnn = copy.deepcopy(rnn) #currently not used.
            best_f1 = res_valid
            test_error = res_test

            #print if there's improvement in training.
            if param['verbose']:
                print('NEW BEST: epoch', e,
                      'valid F1', res_valid,
                      'best test F1', res_test)

        else:
            if param['verbose']:
                print('')

        # learning rate decay if no improvement in 10 epochs
        if param['decay'] and abs(e - be) >= 10:
            lr *= 0.5
            rnn = best_rnn

        if lr < 1e-5:
            break

    print('BEST RESULT: epoch', str(be),
           'valid F1', best_f1,
           'best test F1', test_error)

if __name__ == '__main__':
    test_renet()
